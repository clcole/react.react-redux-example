import React, { Component } from "react";
import { connect } from "react-redux";
import { getData } from "../actions";

const mapStateToProps = state => {
  return { articles: state.remoteArticles.slice(0,10) };
};

class Posts extends Component {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    return (
      <ul>
        {this.props.articles.map(article => 
          <li key={article.id}>{article.title}</li>)}
      </ul>
    );
  }
}

/*
export an anonymous function
will be named during import

use object shorthand form of mapDispatchToProps
is an object of action creators 
react-redux binds the store dispatch to each action creator using bindActionCreators
*/
export default connect(mapStateToProps, {getData})(Posts);
