import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./js/components/App";
import store from "./js/store";

/*
  view -> action -> reducer -> store -> view

  v - the view dispatches an action
  a - an action describes what happened
  r - the reducer receives the current state and action, and returns a new state
  s - the store saves the state and notifies any subscribers to update the view

  mapStateToProps -> view -> mapDispatchToProps
  
  mapStateToProps() - retrieve state in the view layer 
  mapDispatchToProps() - alter state in the view layer
*/

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, 
  document.getElementById('root')
);
